<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pluginspip?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pluginspip_description' => 'Este plugin es la versión 2011 del esqueleto del sitio Plugins SPIP perteneciente a la galaxia SPIP. 
_ Permite, utilizando principalmente el plugin SVP, restituir toda la información de los plugins SPIP en las páginas adaptadas y actualizadas automáticamente.',
	'pluginspip_slogan' => 'Esqueleto Z del sitio Plugins SPIP motorizado por SVP'
);
